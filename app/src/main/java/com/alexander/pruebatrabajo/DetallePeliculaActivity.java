package com.alexander.pruebatrabajo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alexander.pruebatrabajo.model.Abstract;
import com.alexander.pruebatrabajo.model.Genre;
import com.alexander.pruebatrabajo.model.GenreResponse;
import com.alexander.pruebatrabajo.model.Result;
import com.alexander.pruebatrabajo.service.DetalleServicio;
import com.alexander.pruebatrabajo.service.GenreServicio;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetallePeliculaActivity extends AppCompatActivity implements Abstract {

    private Result result;
    private GenreResponse genreResponse;
    private List<Genre> listaGenerosResponse = new ArrayList<>();
    private List<Integer> generos = new ArrayList<>();
    private List<String> textGeneros = new ArrayList<>();
    private final String apiClave = "c600bd2e678ecb8aa8ab11c39e477969";
    private final String URL_IMG = "https://image.tmdb.org/t/p/w500";
    public static final String EXTRA = "FILTER";
    private final String popularSeleccion = "POPULAR";
    private final String tratedSeleccion = "TRATED";
    private final String upcomingSeleccion = "UPCOMING";
    private String peliculaId = "", posterPath, presupuesto, ganancia, listageneros, seleccion;
    private TextView tvNombreOriginal, tvNombre, tvGeneros, tvResumen, tvFecha, tvPuntuacion, tvDuracion, tvPresupuesto, tvGanancia;
    private int idPeliculaint = 0;
    private ImageView imageViewPoster;
    private boolean estadoInternet = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pelicula);

        LinearLayout linearLayout = findViewById(R.id.layoutDetalle);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        validarConexion();

        obtenerGeneros();

        tvNombreOriginal = (TextView) findViewById(R.id.tvNombreOriginalPelicula);
        tvNombre = (TextView) findViewById(R.id.tvNombrePelicula);
        tvGeneros = (TextView) findViewById(R.id.tvGenerosPelicula);
        tvResumen = (TextView) findViewById(R.id.tvResumenPelicula);
        tvFecha = (TextView) findViewById(R.id.tvFechaPelicula);
        tvPuntuacion = (TextView) findViewById(R.id.tvPuntuacionPelicula);
        tvDuracion = (TextView) findViewById(R.id.tvDuracionPelicula);
        tvPresupuesto = (TextView) findViewById(R.id.tvPrseupuestoPelicula);
        tvGanancia = (TextView) findViewById(R.id.tvGananciaPelicula);

        imageViewPoster = (ImageView) findViewById(R.id.ivPosterPelicula);

        peliculaId = obtenerSession("idDetallePelicula");
        seleccion = obtenerSession("seleccionado");
        idPeliculaint = Integer.parseInt(peliculaId);
        detallePelicula(idPeliculaint);
    }

    public void validarConexion() {
        estadoInternet = internetStatus();
        if (!estadoInternet) {
            Intent intentoVolver = new Intent(DetallePeliculaActivity.this,MainActivity.class);
            startActivity(intentoVolver);
            Toast.makeText(getApplicationContext(), R.string.errPeliculaCache, Toast.LENGTH_LONG).show();
        }
    }

    public void detallePelicula(Integer idpelicula) {
        try {
            DetalleServicio detalleServicio = retrofit.create(DetalleServicio.class);
            Call<Result> detallePelicula = detalleServicio.getDetalles(idpelicula, apiClave);
            detallePelicula.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        result = response.body();
                        if (result != null) {
                            llenarVista(result);
                        }
                    }
                }
                @Override
                public void onFailure(Call<Result> call, Throwable t) {}
            });
            try{
                if(detallePelicula.isExecuted() && result == null){
                    notificacionLoad();
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e){}
        } catch (Exception e) {}
    }

    public void llenarVista(Result result){
        String duracionPelicula;
        int hours = result.getRuntime() / 60;
        int minutes = result.getRuntime() % 60;
        duracionPelicula = (hours+"h "+minutes+"m");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
        presupuesto = numberFormat.format(result.getBudget());
        ganancia = numberFormat.format(result.getRevenue());
        for (int i = 0; i < result.getGenres().size(); i++) {
            generos.add(result.getGenres().get(i).getId());
        }
        for (int j = 0; j < generos.size(); j++) {
            for (int k = 0; k < listaGenerosResponse.size(); k++) {
                if (generos.get(j).equals(listaGenerosResponse.get(k).getId())) {
                    textGeneros.add(listaGenerosResponse.get(k).getName());
                }
            }
        }
        listageneros = "";
        for (String s: textGeneros) {
            listageneros += s +", ";
        }
        textGeneros.clear();
        tvNombreOriginal.setText(getString(R.string.detalleOriginal)+" "+result.getOriginalTitle());
        tvNombre.setText(getString(R.string.detalleNombre)+" "+result.getTitle());
        tvGeneros.setText(getString(R.string.detalleGeneros)+" "+listageneros);
        tvResumen.setText(getString(R.string.detalleResumen)+" "+result.getOverview());
        tvFecha.setText(getString(R.string.detalleFecha)+" "+result.getReleaseDate());
        tvPuntuacion.setText(getString(R.string.detallePuntuacion)+" "+result.getVoteAverage()*10+"%");
        tvDuracion.setText(getString(R.string.detalleDuracion)+" "+duracionPelicula);
        tvPresupuesto.setText(getString(R.string.detallePresupuesto)+" "+presupuesto);
        tvGanancia.setText(getString(R.string.detalleGanancia)+" "+ganancia);

        posterPath = URL_IMG+result.getPosterPath();
        try {
            Picasso.get().load(posterPath).fit().centerCrop().into(imageViewPoster);
        } catch (Exception e) {}
    }

    public void obtenerGeneros(){
        try {
            GenreServicio genreServicio = retrofit.create(GenreServicio.class);
            Call<GenreResponse> listaGeneros = genreServicio.getGeneros(apiClave);
            listaGeneros.enqueue(new Callback<GenreResponse>() {
                @Override
                public void onResponse(Call<GenreResponse> call, Response<GenreResponse> response) {
                    if (response.isSuccessful()){
                        genreResponse = response.body();
                        if (genreResponse != null) {
                            listaGenerosResponse = genreResponse.getGenres();
                        }
                    }
                }
                @Override
                public void onFailure(Call<GenreResponse> call, Throwable t) {}
            });
            try{
                if(listaGeneros.isExecuted() && listaGenerosResponse.size() <= 0){
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e){}
        } catch (Exception e){}
    }

    public void notificacionLoad(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View customToast = inflater.inflate(R.layout.load_notificacion,null);
        ProgressBar progressBar = customToast.findViewById(R.id.pbLoad);
        Toast toast = new Toast(DetallePeliculaActivity.this);
        toast.setDuration(toast.LENGTH_LONG);
        toast.setView(customToast);
        toast.show();
    }

    private boolean internetStatus() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //Variables locales
    public void guardarSession (String clave, String valor) {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(clave, valor);
        editor.apply();
    }

    public String obtenerSession (String clave) {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(clave, null);
    }

    public void eliminarSession () {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void eliminarValorSession (String clave) {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(clave);
        editor.commit();
    }
    //Variables locales
}

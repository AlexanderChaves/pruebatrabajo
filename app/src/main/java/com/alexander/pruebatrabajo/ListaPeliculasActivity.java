package com.alexander.pruebatrabajo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alexander.pruebatrabajo.model.Abstract;
import com.alexander.pruebatrabajo.model.Genre;
import com.alexander.pruebatrabajo.model.GenreResponse;
import com.alexander.pruebatrabajo.model.MovieResponse;
import com.alexander.pruebatrabajo.model.Result;
import com.alexander.pruebatrabajo.service.GenreServicio;
import com.alexander.pruebatrabajo.service.PeliculaServicio;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaPeliculasActivity extends AppCompatActivity implements Abstract {

    private boolean estadoInternet = true;
    private final String apiClave = "c600bd2e678ecb8aa8ab11c39e477969";
    private final String popularSeleccion = "POPULAR";
    private final String tratedSeleccion = "TRATED";
    private final String upcomingSeleccion = "UPCOMING";
    private final String filterPopular = "popular";
    private final String filterTrated = "top_rated";
    private final String filterUpcoming = "upcoming";
    private final String generosGuardados = "generos";
    private String seleccion = "", urlAdd = "", resultado = "", idDetallePelicula = "", peliculaGeneros = "", respuesta_guardar = "", peliculas_guardadas = "", generos_guardar = "", generos_guardados = "";
    private Integer idPelicula = 0;
    private int pages = 1;
    private Gson gson;
    private MovieResponse movieResponse, getMovieResponse;
    private GenreResponse genreResponse;
    private List<Integer> generos = new ArrayList<>();
    private ListView lvListaPeliculas;
    private List<Result> listaPeliculasResponse = new ArrayList<>();
    private List<Result> listaPeliculasGuardada = new ArrayList<>();
    private List<Genre> listaGenerosResponse = new ArrayList<>();
    private List<Genre> listaGenerosGuardados = new ArrayList<>();
    private List<String> textGeneros = new ArrayList<>();
    private List<String> peliculasLista = new ArrayList<>();
    private List<Integer> peliculasIdLista = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_peliculas);

        LinearLayout linearLayout = findViewById(R.id.layoutLista);
        AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        lvListaPeliculas = (ListView) findViewById(R.id.lvPeliculas);
        obtenerGeneros();
        Intent extras = getIntent();
        seleccion = extras.getStringExtra(MainActivity.EXTRA);
        validarSeleccion(seleccion);
        lvListaPeliculas.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listaPeliculasResponse.size() > 0) {
                    for (int h = 0; h < peliculasIdLista.size(); h++) {
                        if (position == h) {
                            idDetallePelicula = String.valueOf(peliculasIdLista.get(h));
                            guardarSession("idDetallePelicula", idDetallePelicula);
                            Intent detallePelicula = new Intent(ListaPeliculasActivity.this,DetallePeliculaActivity.class);
                            startActivity(detallePelicula);
                        }
                    }
                }
            }
        });
    }

    public void validarSeleccion(String seleccionado) {
        estadoInternet = internetStatus();
        if (!estadoInternet) {
            obtenerGeneros(generosGuardados);
            switch (seleccionado) {
                case popularSeleccion:
                    obtenerPeliculas(filterPopular);
                    MovieResponse movieResponse1 = new MovieResponse(0, 0, 0, listaPeliculasGuardada);
                    llenarVistaListaOffline(movieResponse1);
                    break;
                case tratedSeleccion:
                    obtenerPeliculas(filterTrated);
                    MovieResponse movieResponse2 = new MovieResponse(0, 0, 0, listaPeliculasGuardada);
                    llenarVistaListaOffline(movieResponse2);
                    break;
                case upcomingSeleccion:
                    obtenerPeliculas(filterUpcoming);
                    MovieResponse movieResponse3 = new MovieResponse(0, 0, 0, listaPeliculasGuardada);
                    llenarVistaListaOffline(movieResponse3);
                    break;
            }
            Toast.makeText(this, R.string.errInternet,Toast.LENGTH_LONG).show();
        } else {
            switch (seleccionado) {
                case popularSeleccion:
                    urlAdd = filterPopular;
                    listarPeliculas(urlAdd);
                    break;
                case tratedSeleccion:
                    urlAdd = filterTrated;
                    listarPeliculas(urlAdd);
                    break;
                case upcomingSeleccion:
                    urlAdd = filterUpcoming;
                    listarPeliculas(urlAdd);
                    break;
            }
        }
    }

    public void listarPeliculas(final String filtro) {
        try {
            PeliculaServicio peliculaServicio = retrofit.create(PeliculaServicio.class);
            Call<MovieResponse> listaPeliculas;
            listaPeliculas = peliculaServicio.getPeliculas(filtro, apiClave, pages);
            listaPeliculas.enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    if (response.isSuccessful()) {
                        movieResponse = response.body();
                        if (movieResponse != null) {
                            llenarVistaLista(movieResponse);
                            guardarPeliculas(movieResponse.getResults(), filtro);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(),R.string.errListaPeliculasVacia,Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),R.string.errConexion,Toast.LENGTH_LONG).show();
                }
            });
            try{
                if(listaPeliculas.isExecuted() && listaPeliculasResponse.size() <= 0){
                    notificacionLoad();
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e){}
        } catch (Exception e) {}
    }

    public void llenarVistaLista(MovieResponse movieResponse){
        if (movieResponse != null) {
            listaPeliculasResponse = movieResponse.getResults();
            if (listaPeliculasResponse.size() > 0) {
                for (int i = 0; i < listaPeliculasResponse.size(); i++) {
                    generos = listaPeliculasResponse.get(i).getGenreIds();
                    if (listaGenerosResponse.size() > 0) {
                        for (int j = 0; j < listaGenerosResponse.size(); j++) {
                            for (int k = 0; k < generos.size(); k++) {
                                if (listaGenerosResponse.get(j).getId() == generos.get(k)) {
                                    textGeneros.add(listaGenerosResponse.get(j).getName());
                                }
                            }
                        }
                    }
                    peliculaGeneros = "";
                    for (String s: textGeneros) {
                        peliculaGeneros += s +", ";
                    }
                    textGeneros.clear();
                    resultado = listaPeliculasResponse.get(i).getOriginalTitle()+" - "+
                            listaPeliculasResponse.get(i).getTitle()+" - "+
                            listaPeliculasResponse.get(i).getReleaseDate()+" - "+
                            peliculaGeneros;
                    idPelicula = listaPeliculasResponse.get(i).getId();
                    peliculasIdLista.add(idPelicula);
                    peliculasLista.add(resultado);
                }
            }
        }
        MyAdapter myAdapter = new MyAdapter(ListaPeliculasActivity.this, R.layout.listapeliculaitem, peliculasLista);
        lvListaPeliculas.setAdapter(myAdapter);
    }

    public void llenarVistaListaOffline(MovieResponse movieResponse){
        if (movieResponse != null) {
            listaPeliculasResponse = movieResponse.getResults();
            if (listaPeliculasResponse.size() > 0) {
                for (int i = 0; i < listaPeliculasResponse.size(); i++) {
                    generos = listaPeliculasResponse.get(i).getGenreIds();
                    if (listaGenerosGuardados.size() > 0) {
                        for (int j = 0; j < listaGenerosGuardados.size(); j++) {
                            for (int k = 0; k < generos.size(); k++) {
                                if (listaGenerosGuardados.get(j).getId() == generos.get(k)) {
                                    textGeneros.add(listaGenerosGuardados.get(j).getName());
                                }
                            }
                        }
                    }
                    peliculaGeneros = "";
                    for (String s: textGeneros) {
                        peliculaGeneros += s +", ";
                    }
                    textGeneros.clear();
                    resultado = listaPeliculasResponse.get(i).getOriginalTitle()+" - "+
                            listaPeliculasResponse.get(i).getTitle()+" - "+
                            listaPeliculasResponse.get(i).getReleaseDate()+" - "+
                            peliculaGeneros;
                    idPelicula = listaPeliculasResponse.get(i).getId();
                    peliculasIdLista.add(idPelicula);
                    peliculasLista.add(resultado);
                }
            }
        }
        MyAdapter myAdapter = new MyAdapter(ListaPeliculasActivity.this, R.layout.listapeliculaitem, peliculasLista);
        lvListaPeliculas.setAdapter(myAdapter);
    }

    public void obtenerGeneros() {
        try {
            GenreServicio genreServicio = retrofit.create(GenreServicio.class);
            Call<GenreResponse> listaGeneros = genreServicio.getGeneros(apiClave);
            listaGeneros.enqueue(new Callback<GenreResponse>() {
                @Override
                public void onResponse(Call<GenreResponse> call, Response<GenreResponse> response) {
                    if (response.isSuccessful()){
                        genreResponse = response.body();
                        if (genreResponse != null) {
                            listaGenerosResponse = genreResponse.getGenres();
                            guardarGeneros(listaGenerosResponse, generosGuardados);
                        }
                    }
                }

                @Override
                public void onFailure(Call<GenreResponse> call, Throwable t) {}
            });
            try{
                if(listaGeneros.isExecuted() && listaGenerosResponse.size() <= 0){
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e){}
        } catch (Exception e) {
        }
    }

    public void guardarPeliculas(List<Result> listaGuardar, String claveLista) {
        gson = new Gson();
        respuesta_guardar = gson.toJson(listaGuardar);
        guardarSession(claveLista, respuesta_guardar);
    }

    public void obtenerPeliculas(String claveLista){
        gson = new Gson();
        peliculas_guardadas = obtenerSession(claveLista);
        Type type = new TypeToken<List<Result>>() {}.getType();
        listaPeliculasGuardada = gson.fromJson(peliculas_guardadas, type);
    }

    public void guardarGeneros(List<Genre> listaGenerosResponse, String claveLista) {
        gson = new Gson();
        generos_guardar = gson.toJson(listaGenerosResponse);
        guardarSession(claveLista, generos_guardar);
    }

    public void obtenerGeneros(String claveLista){
        gson = new Gson();
        generos_guardados = obtenerSession(claveLista);
        Type type = new TypeToken<List<Genre>>() {}.getType();
        listaGenerosGuardados = gson.fromJson(generos_guardados, type);
    }

    public void notificacionLoad(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View customToast = inflater.inflate(R.layout.load_notificacion,null);
        ProgressBar progressBar = customToast.findViewById(R.id.pbLoad);
        Toast toast = new Toast(ListaPeliculasActivity.this);
        toast.setDuration(toast.LENGTH_LONG);
        toast.setView(customToast);
        toast.show();
    }

    //Variables locales
    public void guardarSession (String clave, String valor) {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(clave, valor);
        editor.apply();
    }

    public String obtenerSession (String clave) {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(clave, null);
    }

    public void eliminarSession () {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void eliminarValorSession (String clave) {
        final String MY_PREFERENCES = "Session";
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(clave);
        editor.commit();
    }
    //Variables locales

    private boolean internetStatus() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

package com.alexander.pruebatrabajo;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alexander.pruebatrabajo.model.Abstract;
import com.alexander.pruebatrabajo.model.Result;
import com.alexander.pruebatrabajo.model.MovieResponse;
import com.alexander.pruebatrabajo.service.PeliculaServicio;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Abstract {

    private Button btnPopular, btnTopRated, btnUpcoming;
    public static final String EXTRA = "FILTER";
    public static final String CORTESIA_URL = "https://www.themoviedb.org/assets/1/v4/logos/293x302-powered-by-square-green-3ee4814bb59d8260d51efdd7c124383540fc04ca27d23eaea3a8c87bfa0f388d.png";
    public static final String ICONO_URL = "https://cdn4.iconfinder.com/data/icons/food-and-drink-29/512/20-512.png";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout relativeLayout = findViewById(R.id.layoutMain);
        AnimationDrawable animationDrawable = (AnimationDrawable) relativeLayout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        btnPopular = (Button) findViewById(R.id.btnToPopular);
        btnTopRated = (Button) findViewById(R.id.btnToTopRated);
        btnUpcoming = (Button) findViewById(R.id.btnToUpcoming);

        ImageView cortesiaIv = findViewById(R.id.ivCortesia);
        ImageView iconoIv = findViewById(R.id.ivPresentacion);

        imagenCortesia(cortesiaIv, CORTESIA_URL);
        imagenCortesia(iconoIv, ICONO_URL);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnToPopular:
                Intent intentPopular = new Intent(this, ListaPeliculasActivity.class);
                intentPopular.putExtra(EXTRA, "POPULAR");
                startActivity(intentPopular);
                break;
            case R.id.btnToTopRated:
                Intent intentRated = new Intent(this, ListaPeliculasActivity.class);
                intentRated.putExtra(EXTRA, "TRATED");
                startActivity(intentRated);
                break;
            case R.id.btnToUpcoming:
                Intent intentUpcoming = new Intent(this, ListaPeliculasActivity.class);
                intentUpcoming.putExtra(EXTRA, "UPCOMING");
                startActivity(intentUpcoming);
                break;
        }
    }

    public void imagenCortesia(ImageView imageView, String posterPath){
        try {
            Picasso.get().load(posterPath).fit().centerCrop().into(imageView);
        } catch (Exception e) {}
    }
}

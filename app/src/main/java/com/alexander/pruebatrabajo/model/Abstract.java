package com.alexander.pruebatrabajo.model;

import retrofit2.converter.gson.GsonConverterFactory;

public interface Abstract {
    public static final String baseUrl = "https://api.themoviedb.org/";
    public static final retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}

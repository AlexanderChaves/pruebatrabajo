package com.alexander.pruebatrabajo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    private Context contexto;
    private int layout;
    private List<String> listaPeliculas;

    public MyAdapter(Context contexto, int layout, List<String> peliculasLista) {
        this.contexto = contexto;
        this.layout = layout;
        this.listaPeliculas = peliculasLista;
    }

    @Override
    public int getCount() {
        return listaPeliculas.size();
    }

    @Override
    public Object getItem(int position) {
        return listaPeliculas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(this.contexto);
            convertView = layoutInflater.inflate(R.layout.listapeliculaitem, null);
            viewHolder = new ViewHolder();
            viewHolder.tituloTextView = (TextView) convertView.findViewById(R.id.txt1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String textoActual = listaPeliculas.get(position);
        viewHolder.tituloTextView.setText(textoActual);
        return convertView;
    }

    static class ViewHolder {
        TextView tituloTextView;
    }
}

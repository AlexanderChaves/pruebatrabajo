package com.alexander.pruebatrabajo.service;

import com.alexander.pruebatrabajo.model.GenreResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GenreServicio {

    @GET("3/genre/movie/list")
    Call<GenreResponse> getGeneros(@Query("api_key") String appKey);
}

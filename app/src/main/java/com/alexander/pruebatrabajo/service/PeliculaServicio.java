package com.alexander.pruebatrabajo.service;

import com.alexander.pruebatrabajo.model.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PeliculaServicio {

    @GET("3/movie/{filter}")
    Call<MovieResponse> getPeliculas(@Path("filter") String filter, @Query("api_key") String appKey, @Query("page") Integer page);

}

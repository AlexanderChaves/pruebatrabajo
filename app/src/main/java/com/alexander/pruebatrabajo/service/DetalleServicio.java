package com.alexander.pruebatrabajo.service;

import com.alexander.pruebatrabajo.model.MovieResponse;
import com.alexander.pruebatrabajo.model.Result;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DetalleServicio {
    @GET("3/movie/{movie_id}")
    Call<Result> getDetalles(@Path("movie_id") Integer idpelicula, @Query("api_key") String appKey);
}

# Aplicacion para prueba de trabajo
## por: Jhon Alexander Chaves
#### Capas de la aplicación:
**Persistencia:** ListaPeliculasActivity, DetallePeliculaActivity.
**Negocio:** ListaPeliculasActivity, DetallePeliculaActivity, Genre, Result, GenreResponse, MovieResponse
**Negocio/red:** Abstract, DetalleServicio, GenreServicio, PeliculaServicio.
**Presentación:** MainActivity, ListaPeliculasActivity, DetallePeliculaActivity.
#### Responsabilidades de las clases:
**MainActivity:** Determina el filtro seleccionado por el usuario y lanza otra clase de acuerdo a la selección(ListaPeliculasActivity, DetallePeliculaActivity).
**ListaPeliculasActivity:** Determina el estado de la conexión a internet, obtiene la lista de películas desde el API de acuerdo al filtro seleccionado por el usuario, muestra la lista de películas por filtro, almacena en cache la lista de películas y los generos, de acuerdo a la película seleccionada lanza otra clase.
**DetallePeliculaActivity:** Determina el estado de la conexión a internet, obtiene los datos de la película seleccionada de la lista y muestra los datos de la película.
**Genre, Result, GenreResponse, MovieResponse:** sirven de POJOS para formatear los datos obtenidos por los servicios.
**DetalleServicio, GenreServicio, PeliculaServicio:** sirven de interface para hacer las peticiones al API, determinan el método de petición y sirven para manipular los parámetros y headers.
**Abstract:** sirve de interfaz principal para hacer el llamado al API usando la librería retrofit.
#### Principio de responsabilidad única:
Básicamente se trata de que cada clase creada se encarga únicamente de realizar la menor cantidad de tareas posible, esto a su vez implica que la case debe ser lo más concisa y corta, así mismo, una clase debe estar involucrada en la menor cantidad de capas de la aplicación.
#### Buen código, código limpio: 
Debe ser fácilmente legible por otros programadores, se debe poder hacer trazabilidad de la información, el código debe ser lo más eficaz posible, clases, variables, constantes, métodos etc. Todos deben ser lo mas dicientes de acuerdo a las características de cada uno.
